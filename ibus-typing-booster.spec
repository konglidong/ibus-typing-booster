%define with_check 0
Name:           ibus-typing-booster
Version:        2.16.0
Release:        2
Summary:        A completion input method to speedup typing

License:        GPL-3.0 and Apache-2.0
URL:            https://mike-fabian.github.io/ibus-typing-booster/
Source0:        https://github.com/mike-fabian/ibus-typing-booster/releases/download/%{version}/ibus-typing-booster-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  ibus-devel m17n-db-extras libappstream-glib glib2
BuildRequires:  python3-devel python3-pyxdg python3-enchant
%if %{with_check}
BuildRequires:  m17n-lib hunspell-cs hunspell-de hunspell-en
BuildRequires:  hunspell-es hunspell-fr hunspell-it hunspell-ko
%endif
Requires:       python3 >= 3.3 python3-dbus python3-enchant python3-pyxdg
Requires:       cldr-emoji-annotation unicode-ucd ibus >= 1.5.3 m17n-lib
Recommends:     google-noto-emoji-fonts google-noto-emoji-color-fonts gdouros-symbola-fonts

Provides:       emoji-picker = %{version}-%{release}
Obsoletes:      emoji-picker < %{version}-%{release}

%description
Ibus-typing-booster is a completion input method to speedup typing.

%package_help

%prep
%autosetup -n %{name}-%{version}

%build
export PYTHON=%{__python3}
%configure --disable-additional
%make_build

%install 
export PYTHON=%{__python3}
%make_install NO_INDEX=true  pkgconfigdir=%{_datadir}/pkgconfig

rm $RPM_BUILD_ROOT/%{_datadir}/ibus-typing-booster/data/annotations*/*.xml
rm $RPM_BUILD_ROOT/%{_datadir}/ibus-typing-booster/data/UnicodeData.txt

gzip -n --force --best $RPM_BUILD_ROOT/%{_datadir}/ibus-typing-booster/data/emoji*

%find_lang ibus-typing-booster

%delete_la_and_a

%if %{with_check}
%check
export LC_ALL=en_US.UTF-8
appstream-util validate-relax --nonet %{buildroot}/%{_datadir}/metainfo/*.appdata.xml
desktop-file-validate \
    $RPM_BUILD_ROOT%{_datadir}/applications/ibus-setup-typing-booster.desktop
desktop-file-validate \
    $RPM_BUILD_ROOT%{_datadir}/applications/emoji-picker.desktop
pushd engine
    # run doctests
    python3 hunspell_suggest.py
    python3 m17n_translit.py
    python3 itb_emoji.py
    python3 itb_util.py
popd
mkdir -p /tmp/glib-2.0/schemas/
cp org.freedesktop.ibus.engine.typing-booster.gschema.xml \
   /tmp/glib-2.0/schemas/org.freedesktop.ibus.engine.typing-booster.gschema.xml
glib-compile-schemas /tmp/glib-2.0/schemas #&>/dev/null || :
export XDG_DATA_DIRS=/tmp
eval $(dbus-launch --sh-syntax)
dconf dump /
dconf write /org/freedesktop/ibus/engine/typing-booster/offtherecord false
dconf write /org/freedesktop/ibus/engine/typing-booster/usedigitsasselectkeys true
dconf write /org/freedesktop/ibus/engine/typing-booster/addspaceoncommit true
dconf write /org/freedesktop/ibus/engine/typing-booster/tabenable false
dconf write /org/freedesktop/ibus/engine/typing-booster/inputmethod "'NoIme'"
dconf write /org/freedesktop/ibus/engine/typing-booster/rememberlastusedpreeditime true
dconf write /org/freedesktop/ibus/engine/typing-booster/mincharcomplete 1
dconf write /org/freedesktop/ibus/engine/typing-booster/dictionary "'en_US'"
dconf write /org/freedesktop/ibus/engine/typing-booster/emojipredictions true
dconf write /org/freedesktop/ibus/engine/typing-booster/autocommitcharacters "''"
dconf write /org/freedesktop/ibus/engine/typing-booster/pagesize 6
dconf write /org/freedesktop/ibus/engine/typing-booster/shownumberofcandidates true
dconf write /org/freedesktop/ibus/engine/typing-booster/showstatusinfoinaux true
dconf dump /
ibus-daemon -drx
make check || cat ./tests/test-suite.log
%endif

%post
[ -x %{_bindir}/ibus ] && %{_bindir}/ibus write-cache --system &>/dev/null || :

%postun
[ -x %{_bindir}/ibus ] && %{_bindir}/ibus write-cache --system &>/dev/null || :

%files -f ibus-typing-booster.lang
%defattr(-,root,root)
%doc AUTHORS
%license COPYING
%{_bindir}/emoji-picker
%{_libexecdir}/ibus-*-typing-booster
%{_datadir}/ibus-typing-booster
%{_datadir}/ibus/component/typing-booster.xml
%{_datadir}/metainfo/*.appdata.xml
%{_datadir}/glib-2.0/schemas/org.freedesktop.ibus.engine.typing-booster.gschema.xml
%{_datadir}/icons/hicolor/*
%{_datadir}/applications/*.desktop

%files help
%defattr(-,root,root)
%doc README ChangeLog NEWS


%changelog
* Thu Oct 20 2022 konglidong <konglidong@uniontech.com> - 2.16.0-2
- License compliance rectification

* Tue Jun 21 2022 SimpleUpdate Robot <tc@openeuler.org> - 2.16.0-1
- Upgrade to version 2.16.0

* Thu Nov 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.1.2-3
- Package init
